﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DashboardAPI.Dto
{
  [JsonObject]
  public class UserDto
  {
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public int Id { get; set; }
    [JsonIgnore]
    public string GoogleId { get; set; }
  }
}
