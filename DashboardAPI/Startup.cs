using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Repository.SQLRepositories;

namespace DashboardAPI
{
  public class Startup
  {
    public const string Secret = "akShCgrt5JD6gnd38H3zNrTD63HtKsr5JD6nd385JD6nd38H3zNrTjD63HtH3zqPNrTD63HtKsr5JD6ndp38H3zNrTD63HtKlsr5JD6n";

    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      Services.Module.Init();
      Repository.Module.Init();

      foreach (var dep in IoC.IoC.GetSingletons())
        services.AddSingleton(dep.Key, dep.Value);

      foreach (var dep in IoC.IoC.GetScopes())
        services.AddScoped(dep.Key, dep.Value);

      foreach (var dep in IoC.IoC.GetTransinets())
        services.AddTransient(dep.Key, dep.Value);

      services.AddControllers();

      var key = Encoding.ASCII.GetBytes(Secret);

      services.AddAuthentication(x =>
        {
          x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
          x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        })
        .AddJwtBearer(x =>
        {
          x.RequireHttpsMetadata = true;
          x.SaveToken = true;
          x.TokenValidationParameters = new TokenValidationParameters
          {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(key),
            ValidateIssuer = false,
            ValidateAudience = false
          };
        });

      services.AddDbContext<ApplicationContext>(ServiceLifetime.Scoped);
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

      app.UseHttpsRedirection();

      app.UseAuthentication();
      app.UseAuthorization();

      app.UseRouting();

      app.UseAuthorization();

      app.UseEndpoints(endpoints =>
      {
        endpoints.MapControllers();
      });
    }
  }
}
