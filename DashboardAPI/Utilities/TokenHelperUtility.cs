﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using DashboardAPI.Dto;
using Microsoft.IdentityModel.Tokens;

namespace DashboardAPI.Utilities
{
  public static class TokenHelperUtility
  {
    public static UserDto DecodeGoogleJwtIntoUserDto(string token)
    {
      var userDto = new UserDto();
      var jwtHandler = new JwtSecurityTokenHandler();

      if (!jwtHandler.CanReadToken(token)) return null;

      var securityToken = jwtHandler.ReadJwtToken(token);

      userDto.FirstName = GetClaimValue(securityToken.Claims, "given_name");
      userDto.LastName = GetClaimValue(securityToken.Claims, "family_name");
      userDto.Email = GetClaimValue(securityToken.Claims, "email");
      userDto.GoogleId = GetClaimValue(securityToken.Claims, "sub");

      return userDto;
    }

    public static string GenerateToken(UserDto userDto)
    {
      var tokenHandler = new JwtSecurityTokenHandler();
      var key = Encoding.ASCII.GetBytes(Startup.Secret);

      var tokenDescriptor = new SecurityTokenDescriptor
      {
        Subject = new ClaimsIdentity(new Claim[]
        {
          new Claim("FullName", $"{userDto.FirstName} {userDto.FirstName}"),
          new Claim("Email", userDto.Email),
          new Claim("Id", userDto.Id.ToString()),
          new Claim("Role", "admin")
        }),

        Expires = DateTime.UtcNow.AddDays(7),
        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
      };

      var jwtToken = tokenHandler.CreateToken(tokenDescriptor);

      return tokenHandler.WriteToken(jwtToken);
    }

    private static string GetClaimValue(this IEnumerable<Claim> claims, string type)
    {
      return claims.FirstOrDefault(x => string.Equals(x.Type, type, StringComparison.InvariantCultureIgnoreCase))?.Value;
    }
  }
}
