﻿using System.Collections.Generic;
using Domain.Models.Dto;
using Microsoft.AspNetCore.Mvc;
using Services.CardManagement.Abstractions;

namespace DashboardAPI.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class CardController : ControllerBase
  {
    public CardController(ICardService cardService)
    {
      _cardService = cardService;
    }

    /// <summary>
    /// Get all the cards associated with the dashboard
    /// </summary>
    /// <param name="dashboardId"></param>
    /// <returns>Card list</returns>
    // GET: api/<CardController>/DashboardCards
    [HttpGet("AllDashboardCards/{dashboardId}")]
    public IEnumerable<CardDto> GetDashboardCards(int dashboardId)
    {
      return _cardService.GetAllCardsByDashboardId(dashboardId);
    }

    /// <summary>
    /// Returns card by id
    /// </summary>
    /// <param name="id"></param>
    /// <returns>Card</returns>
    // GET api/<CardController>/5
    [HttpGet("{id}")]
    public CardDto Get(int id)
    {
      return _cardService.GetCard(id);
    }

    /// <summary>
    /// Creates new card
    /// </summary>
    /// <param name="card"></param>
    // POST api/<CardController>
    [HttpPost]
    public void Post([FromBody] CardDto card)
    {
      _cardService.CreateNewCard(card);
    }

    /// <summary>
    /// Modifies existing card
    /// </summary>
    /// <param name="id"></param>
    /// <param name="card"></param>
    // PUT api/<CardController>/5
    [HttpPut("{id}")]
    public void Put(int id, [FromBody] CardDto card)
    {
      _cardService.ModifyCard(id, card);
    }

    /// <summary>
    /// Deletes existing card
    /// </summary>
    /// <param name="id"></param>
    // DELETE api/<CardController>/5
    [HttpDelete("{id}")]
    public void Delete(int id)
    {
      _cardService.DeleteCard(id);
    }

    private readonly ICardService _cardService;
  }
}
