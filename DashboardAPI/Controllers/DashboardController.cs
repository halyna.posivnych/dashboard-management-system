﻿using System.Collections.Generic;
using Domain.Models.Dto;
using Microsoft.AspNetCore.Mvc;
using Services.DashboardManagement.Abstractions;

namespace DashboardAPI.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class DashboardController : ControllerBase
  {
    public DashboardController(IDashboardService dashboardService)
    {
      _dashboardService = dashboardService;
    }

    /// <summary>
    /// Return list of available dashboards
    /// </summary>
    /// <returns>Dashboard list</returns>
    // GET: api/<DashboardController>
    [HttpGet]
    public IEnumerable<DashboardDto> Get()
    {
      return _dashboardService.GetAllDashboardsLite();
    }

    /// <summary>
    /// Returns dashboard by provided id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    // GET api/<DashboardController>/5
    [HttpGet("{id}")]
    public DashboardDto Get(int id)
    {
      return _dashboardService.GetDashboardById(id);
    }

    /// <summary>
    /// Creates new Dashboard
    /// </summary>
    /// <param name="dashboard">Dashboard initial details</param>
    // POST api/<DashboardController>
    [HttpPost]
    public void Post([FromBody] DashboardDto dashboard)
    {
      _dashboardService.CreateNewDashboard(dashboard);
    }

    /// <summary>
    /// Modifies existing dashboard data
    /// </summary>
    /// <param name="id">Dashboard identifier</param>
    /// <param name="dashboard">Dashboard modified details</param>
    // PUT api/<DashboardController>/5
    [HttpPut("{id}")]
    public void Put(int id, [FromBody] DashboardDto dashboard)
    {
      _dashboardService.EditDashboardDetails(id, dashboard);
    }

    /// <summary>
    /// Removes existing dashboard
    /// </summary>
    /// <param name="id">Dashboard identifier</param>
    // DELETE api/<DashboardController>/5
    [HttpDelete("{id}")]
    public void Delete(int id)
    {
      _dashboardService.RemoveDashboard(id);
    }

    private readonly IDashboardService _dashboardService;
  }
}
