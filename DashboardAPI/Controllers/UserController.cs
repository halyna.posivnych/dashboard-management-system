﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using DashboardAPI.Dto;
using DashboardAPI.Utilities;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Services.UserManagement;

namespace DashboardAPI.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class UserController : ControllerBase
  {
    private readonly IUserService _userService;

    public UserController(IUserService userService)
    {
      _userService = userService;
    }

    [HttpPost("register")]
    public void Register([FromBody] UserDto userInfo)
    {
      var newUser = new User()
      {
        Email = userInfo.Email,
        FirstName = userInfo.FirstName,
        LastName = userInfo.LastName
      };

      _userService.SaveOrUpdate(newUser);
    }

    [HttpPost("login")]
    public string Login(string email)
    {
      var userInfo = _userService.GetUserByEmail(email);

      if (userInfo == null)
      {
        return string.Empty;
      }

      var userDto = new UserDto()
      {
        Email = userInfo.Email,
        FirstName = userInfo.FirstName,
        LastName = userInfo.LastName,
        Id = userInfo.Id
      };

      return TokenHelperUtility.GenerateToken(userDto);
    }

    [HttpPost("registerByGoogleToken")]
    public void Register([FromBody] string token)
    {
      var decodedUser = TokenHelperUtility.DecodeGoogleJwtIntoUserDto(token);

      var newUser = new User()
      {
        Email = decodedUser.Email,
        FirstName = decodedUser.FirstName,
        LastName = decodedUser.LastName,
        GoogleId = decodedUser.GoogleId
      };

      _userService.SaveOrUpdate(newUser);
    }

    [HttpPost("googleLogin")]
    public string GoogleLogin([FromBody] string token)
    {
      var decodedUser = TokenHelperUtility.DecodeGoogleJwtIntoUserDto(token);
      if (decodedUser == null)
        return string.Empty;

      var userInfo = _userService.GetUserByEmail(decodedUser.Email);

      if (userInfo == null)
      {
        return string.Empty;
      }

      var userDto = new UserDto()
      {
        Email = userInfo.Email,
        FirstName = userInfo.FirstName,
        LastName = userInfo.LastName,
        Id = userInfo.Id
      };

      return TokenHelperUtility.GenerateToken(userDto);
    }

    [Authorize]
    [HttpGet("info")]
    public UserDto GetProfileInfo(string email)
    {
      var userInfo = _userService.GetUserByEmail(email);

      return new UserDto
      {
        Email = userInfo?.Email,
        FirstName = userInfo?.FirstName,
        LastName = userInfo?.LastName,
        Id = userInfo?.Id ?? 0
      };
    }
  }
}
