﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Models;
using Domain.Repositories;

namespace Repository.InMemoryImplementation
{
  public class ActorInMemoryRepository : IActorRepository
  {
    private static readonly Dictionary<int, Actor> ActorsDictionary = new Dictionary<int, Actor>();
    public Actor Get(int id)
    {
      throw new NotImplementedException();
    }

    public List<Actor> GetAll()
    {
      return ActorsDictionary.Values.ToList();
    }

    public Actor SaveOrUpdate(Actor actor)
    {
      if (actor.Id != 0 && ActorsDictionary.ContainsKey(actor.Id))
      {
        ActorsDictionary[actor.Id] = actor;
      }
      else
      {
        var maxId = ActorsDictionary.Keys.Count > 0 ? ActorsDictionary.Keys.Max() : 0;
        actor.Id = maxId + 1;
        ActorsDictionary.Add(actor.Id, actor);
      }

      return actor;
    }

    public void Delete(Actor actor)
    {
      throw new NotImplementedException();
    }
  }
}
