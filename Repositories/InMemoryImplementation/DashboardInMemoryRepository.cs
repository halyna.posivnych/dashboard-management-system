﻿using System.Collections.Generic;
using System.Linq;
using DashboardAPI.Data.Repositories.Interfaces;
using Domain.Models;

namespace Repository.InMemoryImplementation
{
  public class DashboardInMemoryRepository : IDashboardRepository
  {
    private static readonly Dictionary<int, Dashboard> DashboardCollectionDictionary = new Dictionary<int, Dashboard>();

    public List<Dashboard> GetAll()
    {
      return DashboardCollectionDictionary.Values.ToList();
    }

    public Dashboard Get(int id)
    {
      return DashboardCollectionDictionary.ContainsKey(id) ? DashboardCollectionDictionary[id] : null;
    }

    public Dashboard SaveOrUpdate(Dashboard dashboard)
    {
      var newDashboard = new Dashboard()
      {
        Description = dashboard.Description,
        Name = dashboard.Name
      };

      if (dashboard.Id != 0 && DashboardCollectionDictionary.ContainsKey(dashboard.Id))
      {
        newDashboard.Id = dashboard.Id;
        DashboardCollectionDictionary[dashboard.Id] = newDashboard;
      }
      else
      {
        var maxId = DashboardCollectionDictionary.Keys.Count > 0 ? DashboardCollectionDictionary.Keys.Max() : 0;
        newDashboard.Id = maxId + 1;
        DashboardCollectionDictionary.Add(newDashboard.Id, newDashboard);
      }

      return newDashboard;
    }

    public void Delete(Dashboard dashboard)
    {
      if (DashboardCollectionDictionary.ContainsKey(dashboard.Id))
        DashboardCollectionDictionary.Remove(dashboard.Id);
    }
  }
}
