﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Models;
using Domain.Repositories;

namespace Repository.InMemoryImplementation
{
  public class UserInMemoryRepository : IUserRepository
  {
    private static readonly Dictionary<int, User> UserDictionary = new Dictionary<int, User>();
    public User Get(int id)
    {
      throw new NotImplementedException();
    }

    public List<User> GetAll()
    {
      return UserDictionary.Values.ToList();
    }

    public User SaveOrUpdate(User user)
    {
      if (user.Id != 0 && UserDictionary.ContainsKey(user.Id))
      {
        UserDictionary[user.Id] = user;
      }
      else
      {
        var maxId = UserDictionary.Keys.Count > 0 ? UserDictionary.Keys.Max() : 0;
        user.Id = maxId + 1;
        UserDictionary.Add(user.Id, user);
      }

      return user;
    }

    public void Delete(User user)
    {
      throw new NotImplementedException();
    }
  }
}
