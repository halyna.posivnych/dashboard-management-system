﻿using System.Collections.Generic;
using System.Linq;
using DashboardAPI.Data.Repositories.Interfaces;
using Domain.Models;

namespace Repository.InMemoryImplementation
{
  public class CardInMemoryRepository : ICardRepository
  {
    private static readonly Dictionary<int, Card> CardCollectionDictionary = new Dictionary<int, Card>();

    public List<Card> GetAll()
    {
      return CardCollectionDictionary.Values.ToList();
    }

    public Card Get(int id)
    {
      return CardCollectionDictionary.ContainsKey(id) ? CardCollectionDictionary[id] : null;
    }

    public Card SaveOrUpdate(Card card)
    {
      var newCard = new Card()
      {
        DashboardId = card.DashboardId,
        Description = card.Description,
        Name = card.Name,
        StatusType = card.StatusType
      };

      if (card.Id != 0 && CardCollectionDictionary.ContainsKey(card.Id))
      {
        newCard.Id = card.Id;
        CardCollectionDictionary[card.Id] = newCard;
      }
      else
      {
        var maxId = CardCollectionDictionary.Keys.Count > 0 ? CardCollectionDictionary.Keys.Max() : 0;
        newCard.Id = maxId + 1;
        CardCollectionDictionary.Add(newCard.Id, newCard);
      }

      return newCard;
    }

    public void Delete(Card card)
    {
      if (CardCollectionDictionary.ContainsKey(card.Id))
        CardCollectionDictionary.Remove(card.Id);
    }
  }
}
