﻿using IoC;
using System;
using System.Collections.Generic;
using System.Text;
using DashboardAPI.Data.Repositories.Interfaces;
using Domain.Repositories;
using Repository.SQLRepositories;

namespace Repository
{
  public class Module
  {
    public static void Init()
    {
      IoC.IoC.AddScopped<ICardRepository, CardRepository>();
      IoC.IoC.AddScopped<IDashboardRepository, DashboardRepository>();
      IoC.IoC.AddScopped<IActorRepository, ActorRepository>();
      IoC.IoC.AddScopped<IUserRepository, UserRepository>();
    }
  }
}
