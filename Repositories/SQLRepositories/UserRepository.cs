﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Models;
using Domain.Repositories;

namespace Repository.SQLRepositories
{
  class UserRepository : IUserRepository
  {
    private readonly ApplicationContext _context;

    public UserRepository(ApplicationContext context)
    {
      _context = context;
    }

    public User Get(int id)
    {
      return _context.Users.Find(id);
    }

    public List<User> GetAll()
    {
      return _context.Users.ToList();
    }

    public User SaveOrUpdate(User user)
    {
      if (_context.Users.Find(user.Id) == null)
      {
        _context.Users.Add(user);
      }
      else
      {
        _context.Users.Update(user);
      }
      _context.SaveChanges();

      return user.Id > 0 ? user : null;
    }

    public void Delete(User user)
    {
      throw new NotImplementedException();
    }
  }
}
