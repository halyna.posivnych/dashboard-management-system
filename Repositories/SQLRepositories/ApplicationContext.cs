﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Repository.SQLRepositories
{
  public class ApplicationContext : DbContext
  {
    public ApplicationContext(){}

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      optionsBuilder.UseSqlServer("Server=EPUALVIW030D;Database=EFCore-DashboardApplicationDB;Trusted_Connection=True");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      // User - actor
      modelBuilder.Entity<User>()
        .HasOne<Actor>(u => u.Actor)
        .WithOne(a => a.User)
        .HasForeignKey<Actor>(a => a.UserId);

      // Dashboard - actor (owner)
      modelBuilder.Entity<Dashboard>()
        .HasOne<Actor>(d => d.DashboardOwner)
        .WithMany(a => a.OwnDashboardCollection)
        .HasForeignKey(d => d.DashboardOwnerId);

      // Card - actor (owner)
      modelBuilder.Entity<Card>()
        .HasOne<Actor>(c => c.CardOwner)
        .WithMany(a => a.OwnCardCollection)
        .HasForeignKey(c => c.CardOwnerId);

      // Card - actor (assignee)
      modelBuilder.Entity<Card>()
        .HasOne<Actor>(c => c.CardAssignee)
        .WithMany(a => a.AssignedCardCollection)
        .HasForeignKey(c => c.CardAssigneeId);

      // Card - dashboard
      modelBuilder.Entity<Card>()
        .HasOne<Dashboard>(c => c.Dashboard)
        .WithMany(d => d.CardCollection)
        .HasForeignKey(c => c.DashboardId)
        .OnDelete(DeleteBehavior.Restrict);
    }

    public DbSet<User> Users { get; set; }
    public DbSet<Actor> Actors { get; set; }
    public DbSet<Dashboard> Dashboards { get; set; }
    public DbSet<Card> Cards { get; set; }
  }
}
