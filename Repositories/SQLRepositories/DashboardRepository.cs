﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DashboardAPI.Data.Repositories.Interfaces;
using Domain.Models;

namespace Repository.SQLRepositories
{
  public class DashboardRepository : IDashboardRepository
  {
    private readonly ApplicationContext _context;

    public DashboardRepository(ApplicationContext context)
    {
      _context = context;
    }

    public List<Dashboard> GetAll()
    {
      return _context.Dashboards.ToList();
    }

    public Dashboard Get(int id)
    {
      return _context.Dashboards.Find(id);
    }

    public Dashboard SaveOrUpdate(Dashboard dashboard)
    {
      if (_context.Dashboards.Find(dashboard.Id) == null)
      {
        _context.Dashboards.Add(dashboard);
      }
      else
      {
        _context.Dashboards.Update(dashboard);
      }
      _context.SaveChanges();

      return dashboard.Id > 0 ? dashboard : null;
    }

    public void Delete(Dashboard dashboard)
    {
      throw new NotImplementedException();
    }
  }
}
