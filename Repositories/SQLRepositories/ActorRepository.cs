﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Models;
using Domain.Repositories;

namespace Repository.SQLRepositories
{
  public class ActorRepository : IActorRepository
  {
    private readonly ApplicationContext _context;

    public ActorRepository(ApplicationContext context)
    {
      _context = context;
    }

    public Actor Get(int id)
    {
      return _context.Actors.Find(id);
    }

    public List<Actor> GetAll()
    {
      return _context.Actors.ToList();
    }

    public Actor SaveOrUpdate(Actor actor)
    {
      if (_context.Actors.Find(actor.Id) == null)
      {
        _context.Actors.Add(actor);
      }
      else
      {
        _context.Actors.Update(actor);
      }
      _context.SaveChanges();

      return actor.Id > 0 ? actor : null;
    }

    public void Delete(Actor actor)
    {
      throw new NotImplementedException();
    }
  }
}
