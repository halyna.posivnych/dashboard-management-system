﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DashboardAPI.Data.Repositories.Interfaces;
using Domain.Models;

namespace Repository.SQLRepositories
{
  public class CardRepository : ICardRepository
  {
    private readonly ApplicationContext _context;

    public CardRepository(ApplicationContext context)
    {
      _context = context;
    }

    public List<Card> GetAll()
    {
      return _context.Cards.ToList();
    }

    public Card Get(int id)
    {
      return _context.Cards.Find(id);
    }

    public Card SaveOrUpdate(Card card)
    {
      if (_context.Cards.Find(card.Id) == null)
      {
        _context.Cards.Add(card);
      }
      else
      {
        _context.Cards.Update(card);
      }
      _context.SaveChanges();

      return card.Id > 0 ? card : null;
    }

    public void Delete(Card card)
    {
      throw new NotImplementedException();
    }
  }
}
