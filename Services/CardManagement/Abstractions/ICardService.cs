﻿using System.Collections.Generic;
using Domain.Models.Dto;

namespace Services.CardManagement.Abstractions
{
  public interface ICardService
  {
    IList<CardDto> GetAllCardsByDashboardId(int dashboardId);
    CardDto GetCard(int id);
    CardDto CreateNewCard(CardDto card);
    CardDto ModifyCard(int id, CardDto card);
    void DeleteCard(int id);
    void SetCardOwner(int cardId, int ownerId);
    void SetCardAssignee(int cardId, int ownerId, int? assigneeId = null);
  }
}
