﻿using System.Collections.Generic;
using System.Linq;
using DashboardAPI.Data.Repositories.Interfaces;
using Domain.Models;
using Domain.Models.Dto;
using Domain.Repositories;
using Repository.InMemoryImplementation;
using Services.CardManagement.Abstractions;

namespace Services.CardManagement.Implementation
{
  public class CardManagementService : ICardService
  {
    private readonly ICardRepository _cardRepository;
    private readonly IActorRepository _actorRepository;

    public CardManagementService(ICardRepository cardRepository, IActorRepository actorRepository)
    {
      _cardRepository = cardRepository;
      _actorRepository = actorRepository;
    }

    public IList<CardDto> GetAllCardsByDashboardId(int dashboardId)
    {
      return _cardRepository.GetAll().Where(c => c.DashboardId == dashboardId).Select(c => c.CreateDto()).ToList();
    }

    public CardDto GetCard(int id)
    {
      return _cardRepository.Get(id).CreateDto();
    }

    public CardDto CreateNewCard(CardDto card)
    {
      var newCard = new Card
      {
        DashboardId = card.DashboardId,
        Description = card.Description,
        Name = card.Name,
        StatusType = card.StatusType
      };

      return _cardRepository.SaveOrUpdate(newCard).CreateDto();
    }

    public CardDto ModifyCard(int id, CardDto card)
    {
      var newCard = new Card
      {
        Id = id,
        DashboardId = card.DashboardId,
        Description = card.Description,
        Name = card.Name,
        StatusType = card.StatusType
      };

      return _cardRepository.SaveOrUpdate(newCard).CreateDto();
    }

    public void DeleteCard(int id)
    {
      var existingCard = _cardRepository.Get(id);
      if (existingCard != null)
      {
        _cardRepository.Delete(existingCard);
      }
    }

    public void SetCardOwner(int cardId, int ownerId)
    {
      var card = _cardRepository.Get(cardId);
      var owner = _actorRepository.Get(ownerId);
      if (card != null)
      {
        owner?.SetCardOwner(card);
        _cardRepository.SaveOrUpdate(card);
      }
    }

    public void SetCardAssignee(int cardId, int ownerId, int? assigneeId = null)
    {
      var card = _cardRepository.Get(cardId);
      var owner = _actorRepository.Get(ownerId);
      if (card != null)
      {
        if (assigneeId.HasValue)
          owner?.ChangeCardAssignment(card, assigneeId.Value);
        else
          owner.AssignToMyself(card);

        _cardRepository.SaveOrUpdate(card);
      }
    }
  }
}
