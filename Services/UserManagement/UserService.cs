﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Models;
using Domain.Repositories;
using Repository.InMemoryImplementation;

namespace Services.UserManagement
{
  public class UserService : IUserService
  {
    private readonly IUserRepository _userRepository;

    public UserService(IUserRepository userRepository)
    {
      _userRepository = userRepository;
    }

    public User GetUserByEmail(string email)
    {
      return _userRepository.GetAll().FirstOrDefault(u => u.Email == email);
    }

    public User SaveOrUpdate(User user)
    {
      return _userRepository.SaveOrUpdate(user);
    }
  }
}
