﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Models;

namespace Services.UserManagement
{
  public interface IUserService
  {
    User GetUserByEmail(string email);
    User SaveOrUpdate(User user);
  }
}
