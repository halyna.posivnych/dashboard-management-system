﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Models;
using Domain.Repositories;
using Repository.InMemoryImplementation;
using Services.ActorManagement.Abstractions;

namespace Services.ActorManagement.Implementation
{
  public class ActorService : IActorService
  {
    private readonly IActorRepository _actorRepository;

    public ActorService(IActorRepository actorRepository)
    {
      _actorRepository = actorRepository;
    }

    public Actor GetActorByEmail(string email)
    {
      return _actorRepository.GetAll().FirstOrDefault(a => a.Email == email);
    }

    public Actor SaveOrUpdate(Actor actor)
    {
      return _actorRepository.SaveOrUpdate(actor);
    }
  }
}
