﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Models;

namespace Services.ActorManagement.Abstractions
{
  public interface IActorService
  {
    Actor GetActorByEmail(string email);
    Actor SaveOrUpdate(Actor actor);
  }
}
