﻿using System.Collections.Generic;
using Domain.Models.Dto;

namespace Services.DashboardManagement.Abstractions
{
  public interface IDashboardService
  {
    IList<DashboardDto> GetAllDashboardsLite();
    DashboardDto GetDashboardById(int id);
    void RemoveDashboard(int id);
    DashboardDto EditDashboardDetails(int id, DashboardDto dashboard);
    void CreateNewDashboard(DashboardDto dashboard);
    void SetDashboardOwner(int dashboardId, int actorId);
  }
}
