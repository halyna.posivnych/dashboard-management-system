﻿using System.Collections.Generic;
using System.Linq;
using DashboardAPI.Data.Repositories.Interfaces;
using Domain.Models;
using Domain.Models.Dto;
using Domain.Repositories;
using Repository.InMemoryImplementation;
using Services.DashboardManagement.Abstractions;

namespace Services.DashboardManagement.Implementation
{
  public class DashboardManagementService : IDashboardService
  {
    private readonly IDashboardRepository _dashboardRepository;
    private readonly ICardRepository _cardRepository;
    private readonly IActorRepository _actorRepository;

    public DashboardManagementService(ICardRepository cardRepository, IDashboardRepository dashboardRepository, IActorRepository actorRepository)
    {
      _cardRepository = cardRepository;
      _dashboardRepository = dashboardRepository;
      _actorRepository = actorRepository;
    }

    public IList<DashboardDto> GetAllDashboardsLite()
    {
      return _dashboardRepository.GetAll().Select(x => x.CreateDto()).ToList();
    }

    public DashboardDto GetDashboardById(int id)
    {
      return _dashboardRepository.Get(id)?.CreateDto();
    }

    public void RemoveDashboard(int id)
    {
      var existingDashboard = _dashboardRepository.Get(id);

      if (existingDashboard == null) return;

      _dashboardRepository.Delete(existingDashboard);
      var cards = _cardRepository.GetAll().Where(c => c.DashboardId == id);
      foreach (var card in cards)
      {
        _cardRepository.Delete(card);
      }
    }

    public DashboardDto EditDashboardDetails(int id, DashboardDto dashboard)
    {
      var newDashboard = new Dashboard()
      {
        Id = id,
        Description = dashboard.Description,
        Name = dashboard.Name
      };

      return _dashboardRepository.SaveOrUpdate(newDashboard).CreateDto();
    }

    public void CreateNewDashboard(DashboardDto dashboard)
    {
      var newDashboard = new Dashboard()
      {
        Description = dashboard.Description,
        Name = dashboard.Name
      };

      _dashboardRepository.SaveOrUpdate(newDashboard).CreateDto();
    }

    public void SetDashboardOwner(int dashboardId, int actorId)
    {
      var actor = _actorRepository.Get(actorId);
      var dashboard = _dashboardRepository.Get((dashboardId));

      if(dashboard != null)
        actor?.SetDashboardOwner(dashboard);
    }
  }
}
