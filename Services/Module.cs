﻿using Services.ActorManagement.Abstractions;
using Services.ActorManagement.Implementation;
using Services.CardManagement.Abstractions;
using Services.CardManagement.Implementation;
using Services.DashboardManagement.Abstractions;
using Services.DashboardManagement.Implementation;
using Services.UserManagement;

namespace Services
{
  public static class Module
  {
    public static void Init()
    {
      IoC.IoC.AddScopped<ICardService, CardManagementService>();
      IoC.IoC.AddScopped<IDashboardService, DashboardManagementService>();
      IoC.IoC.AddScopped<IActorService, ActorService>();
      IoC.IoC.AddScopped<IUserService, UserService>();
    }
  }
}
