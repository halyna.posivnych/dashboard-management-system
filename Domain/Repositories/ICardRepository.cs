﻿using System.Collections.Generic;
using Domain.Models;

namespace DashboardAPI.Data.Repositories.Interfaces
{
  public interface ICardRepository
  {
    List<Card> GetAll();
    Card Get(int id);
    Card SaveOrUpdate(Card card);
    void Delete(Card card);
  }
}
