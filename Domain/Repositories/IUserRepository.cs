﻿using System.Collections.Generic;
using Domain.Models;

namespace Domain.Repositories
{
  public interface IUserRepository
  {
    User Get(int id);
    List<User> GetAll();
    User SaveOrUpdate(User user);
    void Delete(User user);
  }
}
