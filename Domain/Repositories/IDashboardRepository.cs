﻿using System.Collections.Generic;
using Domain.Models;

namespace DashboardAPI.Data.Repositories.Interfaces
{
  public interface IDashboardRepository
  {
    List<Dashboard> GetAll();
    Dashboard Get(int id);
    Dashboard SaveOrUpdate(Dashboard dashboard);
    void Delete(Dashboard dashboard);
  }
}
