﻿using System.Collections.Generic;
using Domain.Models;

namespace Domain.Repositories
{
  public interface IActorRepository
  {
    Actor Get(int id);
    List<Actor> GetAll();
    Actor SaveOrUpdate(Actor actor);
    void Delete(Actor actor);
  }
}
