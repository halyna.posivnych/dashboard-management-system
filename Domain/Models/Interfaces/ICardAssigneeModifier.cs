﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.Interfaces
{
  public interface ICardAssigneeModifier
  {
    void AssignToMyself(Card card);
    void ChangeCardAssignment(Card card, int assigneeId);
  }
}
