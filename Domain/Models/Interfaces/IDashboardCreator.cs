﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.Interfaces
{
  public interface IDashboardCreator
  {
    void SetDashboardOwner(Dashboard dashboard);
  }
}
