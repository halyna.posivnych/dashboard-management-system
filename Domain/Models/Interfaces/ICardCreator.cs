﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.Interfaces
{
  public interface ICardCreator
  {
    void SetCardOwner(Card card);
  }
}
