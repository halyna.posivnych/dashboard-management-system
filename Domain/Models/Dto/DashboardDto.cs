﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Enums;

namespace Domain.Models.Dto
{
  public class DashboardDto
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }

    public CardDto Cards { get; set; }
  }
}
