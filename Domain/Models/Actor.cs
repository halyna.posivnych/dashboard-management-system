﻿using System.Collections.Generic;
using Domain.Models.Interfaces;

namespace Domain.Models
{
  public class Actor : ICardCreator, IDashboardCreator, ICardAssigneeModifier
  {
    public int Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }

    public int UserId { get; set; }
    public User User { get; set; }

    public List<Dashboard> OwnDashboardCollection { get; set; }

    public List<Card> OwnCardCollection { get; set; }

    public List<Card> AssignedCardCollection { get; set; }

    public void SetCardOwner(Card card)
    {
      card.CardOwnerId = Id;
    }

    public void SetDashboardOwner(Dashboard dashboard)
    {
      dashboard.DashboardOwnerId = Id;
    }

    public void AssignToMyself(Card card)
    {
      card.CardAssigneeId = Id;
    }

    public void ChangeCardAssignment(Card card, int assigneeId)
    {
      card.CardAssigneeId = assigneeId;
    }
  }
}
