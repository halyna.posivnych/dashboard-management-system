﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
  public class User
  {
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public int Id { get; set; }
    public string GoogleId { get; set; }

    public Actor Actor { get; set; }
  }
}
