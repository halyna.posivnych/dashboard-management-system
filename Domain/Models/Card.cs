﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Enums;
using Domain.Models.Dto;

namespace Domain.Models
{
  public class Card
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public CardStatusType StatusType { get; set; } 

    public int DashboardId { get; set; }
    public Dashboard Dashboard { get; set; }

    public int CardOwnerId { get; set; }
    public Actor CardOwner { get; set; }

    public int? CardAssigneeId { get; set; }
    public Actor CardAssignee { get; set; }

    public CardDto CreateDto()
    {
      return new CardDto()
      {
        Id = Id,
        DashboardId = DashboardId,
        Description = Description,
        Name = Name,
        StatusType = StatusType
      };
    }
  }
}
