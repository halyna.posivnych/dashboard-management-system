﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Models.Dto;

namespace Domain.Models
{
  public class Dashboard
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }

    public int DashboardOwnerId { get; set; }
    public Actor DashboardOwner { get; set; }

    public IList<Card> CardCollection { get; set; }

    public DashboardDto CreateDto()
    {
      return new DashboardDto
      {
        Id = Id,
        Description = Description,
        Name = Name
      };
    }
  }
}
