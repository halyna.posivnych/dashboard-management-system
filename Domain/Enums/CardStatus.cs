﻿namespace Domain.Enums
{
  public enum CardStatusType
  {
    ToDo,
    InProgress,
    Done
  }
}
