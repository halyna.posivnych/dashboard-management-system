using System.Linq;
using DashboardAPI.Controllers;
using Domain.Models.Dto;
using NUnit.Framework;
using Repository.InMemoryImplementation;
using Repository.SQLRepositories;
using Services.DashboardManagement.Implementation;

namespace NUnitTestProject
{
  public class DashboardControllerTests
  {
    private DashboardController _dashboardController;

    [SetUp]
    public void Setup()
    {
      _dashboardController = new DashboardController(new DashboardManagementService(new CardInMemoryRepository(), new DashboardInMemoryRepository(), new ActorInMemoryRepository()));
      _dashboardController.Post(
        new DashboardDto
        {
          Description = "The first dashboard description",
          Name = "#1 Dashboard"
        });
    }

    [Test]
    public void TestGetDashboard()
    {
      var result = _dashboardController.Get();

      Assert.IsNotEmpty(result);
    }

    [Test]
    public void TestPutAndGetDashboard()
    {
      _dashboardController.Post(
        new DashboardDto
        {
          Description = "The second dashboard description",
          Name = "#2 Dashboard"
        });

      var result = _dashboardController.Get().ToList();
      Assert.IsNotEmpty(result);
      Assert.IsNotNull(result.FirstOrDefault(d => d.Name == "#2 Dashboard"));
    }

    [Test]
    public void TestGetByIdDashboard()
    {
      var result = _dashboardController.Get(1);

      Assert.IsNotNull(result);
      Assert.Pass();
    }
  }
}