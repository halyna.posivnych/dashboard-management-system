﻿using DashboardAPI.Controllers;
using Domain.Enums;
using Domain.Models.Dto;
using NUnit.Framework;
using Repository.InMemoryImplementation;
using Services.CardManagement.Implementation;

namespace NUnitTestProject
{
  public class CardControllerTests
  {
    private CardController _cardController;

    [SetUp]
    public void Setup()
    {
      _cardController = new CardController(new CardManagementService(new CardInMemoryRepository(), new ActorInMemoryRepository()));
      _cardController.Post(new CardDto
      {
        DashboardId = 1,
        Description = "The first card description",
        Name = "#1 card",
        StatusType = CardStatusType.ToDo
      });
      _cardController.Post(new CardDto
      {
        DashboardId = 2,
        Description = "The second card description",
        Name = "#1 card",
        StatusType = CardStatusType.ToDo
      });
    }

    [Test]
    public void TestGetCard()
    {
      var result = _cardController.Get(1);

      Assert.IsNotNull(result);
    }

    [Test]
    public void TestPutAndGetCard()
    {
      var updatedCard = new CardDto
      {
        Id = 1,
        DashboardId = 1,
        Description = "The first card description [edit]",
        Name = "#1 card",
        StatusType = CardStatusType.InProgress
      };

      _cardController.Put(1, updatedCard);
      var result = _cardController.Get(1);

      Assert.IsNotNull(result);
      Assert.AreEqual(updatedCard.Description, result.Description);
      Assert.AreEqual(updatedCard.Name, result.Name);
      Assert.AreEqual(updatedCard.StatusType, result.StatusType);
      Assert.AreEqual(updatedCard.Id, result.Id);
    }
  }
}
